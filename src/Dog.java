public class Dog extends Pet implements Foulable{
    public Dog(String nickname){
        super(nickname);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я собака " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я собака і я роблю гидоти...");
    }
}