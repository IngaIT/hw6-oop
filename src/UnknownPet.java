
    public class UnknownPet extends Pet {
        public UnknownPet(String nickname) {
            super(nickname);
            setSpecies(Species.UNKNOWN);
        }

        public UnknownPet(String nickname, int age, int trickLevel, String[] habits) {
            super(nickname, age, trickLevel, habits);
            setSpecies(Species.UNKNOWN);
        }

    }

