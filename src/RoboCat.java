public class RoboCat extends Pet implements Foulable{
    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.RCAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.RCAT);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я робокіт " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я робокіт і я роблю гидоти...");
    }
}