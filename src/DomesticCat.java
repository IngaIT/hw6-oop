public class DomesticCat extends Pet implements Foulable{
    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.CAT);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.CAT);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я кіт " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я кіт і я роблю гидоти...");
    }
}