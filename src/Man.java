public final class Man extends Human{
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, DaySchedule[] schedule) {
        super(name, surname, year, iq, schedule);
    }

    void repairCar(){
        System.out.println("Полагодити авто");
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ", як справи, друже?");
    }
}
