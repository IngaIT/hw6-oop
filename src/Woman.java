public final class Woman extends Human{
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, DaySchedule[] schedule) {
        super(name, surname, year, iq, schedule);
    }

    void makeup(){
        System.out.println("Підфарбуватися");
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ", ти така мила киця!");
    }
}
